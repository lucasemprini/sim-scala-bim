As a <Team Role>, 
I want to <desired action>, 
so that <desired benefit>.

(Brief simple statements of research that is needed in order to move forward with other items in the Product Backlog)

/label ~Spike