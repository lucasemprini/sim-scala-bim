As a <Team Role>, 
I want to <desired action>, 
so that <desired benefit>

(Brief simple statements of foundational or infrastructure needed in order to deliver User Stories in the Product Backlog)

/label ~"Non\-User Story"