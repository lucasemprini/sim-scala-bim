# SIM-ScALA-BIM #

This project an [Abraca-what?](https://boardgamegeek.com/boardgame/163930/abracadawhat) game clone written in Scala for educational purpose.

## Project description ##

This project is developed as final project elaboration for the exam "Paradigmi di Programmazione e Sviluppo" (PPS) of the Master’s Degree in Computer Science and Engeneering at University of Bologna, Campus of Cesena.
According to the exam rules, the project is developed on JVM platform (preferably Scala and tuProlog) and the development flow should be based on SCRUM and use tools like a CI platform.
For more details, see [RULES.md file](./RULES.md).

### Project request post (in italian) ###

> Email:
> * niccolo.maltoni@studio.unibo.it
> * luca.semprini10@studio.unibo.it
> * riccardo.soro@studio.unibo.it
> * lorenzo.valgimigli@studio.unibo.it
> 
> Principali aspetti del processo di sviluppo:
> 
> * Scrum (durata sprint da valutare, ipotizzato sprint bisettimanali)
> * GitLab
> * GitLab CI / GitLab Auto DevOps
> * GitLab Boards
> * Gradle
> * Git
> 
> Metodologia di sviluppo del progetto:
> 
> * Il lavoro verrà strutturato seguendo un approccio Agile, ispirato a Scrum
> * Non sarà presente uno scrum master
> * Durante lo sprint planning verranno definiti i compiti per ognuno dei membri per lo sprint corrente
> * Al termine di ogni sprint verrà tenuta una riunione tra i membri del gruppo per effettuare le operazioni di Sprint Review e Sprint Retrospective
>
> Requisiti del sistema di massima da realizzare:
> Verrà implementata una versione multiplayer distribuita del gioco da tavolo Abraca-Boh.
>
> Regole su cui si basa Abraca-Boh:
> > “Ogni giocatore ha a disposizione 5 pietre magiche, ma può vedere soltanto gli incantesimi in possesso degli altri giocatori e NON i suoi. I giocatori pronunciano il nome dell’incantesimo che vogliono lanciare, e se hanno la pietra magica corrispondente, l’effetto sarà attivato. Se non hanno la pietra magica corrispondente, perderanno dei punti vita. È possibile incrementare la precisione degli incantesimi da lanciare deducendo la probabilità di essere in possesso di certi incantesimi: l’ammontare di pietre magiche di un determinato incantesimo è noto, e i giocatori possono sempre vedere le pietre magiche degli altri giocatori. I giocatori continuano a cercare di lanciare incantesimi finché qualcuno non viene eliminato o non usa tutte le sue pietre magiche. Alla fine del round, i giocatori possono salire lungo la torre in base al risultato ottenuto. Il primo giocatore che arriva in cima alla torre vince la partita (cosa che richiederà più di 1 round).” - descrizione da Amazon.it
> 
> Requisiti del sistema di massima da realizzare:
>
> * Implementazione delle regole di base di Abraca-Boh
> * Modalità multiplayer distribuita su un’architettura client-server, ed ogni partita avviene
> * Interfaccia grafica 2D con la rappresentazione dal punto di vista del client della partita
> * Il sistema gestisce l’autenticazione sul server dei singoli utenti, i quali possono salvare altri utenti come amici
> * L’utente ha la possibilità di effettuare partite sia con altri giocatori da lui scelti, sia con avversari/compagni casuali
> * Il sistema gestisce più partite in contemporanea e tiene traccia delle partite avvenute
>
> Feature opzionali:
> 
> * Possibilità di gioco con IA semplificata
> * Chat durante la partita
> * Possibilità di ricercare una partita e osservarla da spettatore

## License & Copyright ##

We do not own any copyright about the original Abraca-What? game; every trademark is owned by the respective owner.

Our code is licensed under the license specified in the [specific file](./LICENSE).